package com.example.latter.delegates;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.yokeyword.fragmentation_swipeback.SwipeBackFragment;

/**
 * Created by cc on 2017/9/5.
 */

public abstract class BaseDelegate extends SwipeBackFragment {

    private Unbinder mUnbinder = null;

    public abstract Object setLayOut();
    public abstract void bindView(Bundle savedInstanceState,View view);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = null;
        if (setLayOut() instanceof Integer) {
            rootView = inflater.inflate((Integer) setLayOut(), container, false);
        } else if (setLayOut() instanceof View) {
            rootView = (View) setLayOut();
        } else {
            throw new ClassCastException("SetLayout must be Integer or View!");
        }

        if (rootView != null){
            mUnbinder = ButterKnife.bind(this,rootView);
            bindView(savedInstanceState,rootView);
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null){
            mUnbinder.unbind();
        }
    }
}
