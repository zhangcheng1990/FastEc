package com.example.latter.app;

import android.content.Context;

import java.util.WeakHashMap;

/**
 * Created by cc on 2017/9/5.
 */

public final class Latte {

    public static Configurator init(Context context){
        getConfigurations().put(ConfigType.APPLICATION_CONTEXT.name(),context.getApplicationContext());
        return Configurator.getInstance();
    }


    private static WeakHashMap<String,Object> getConfigurations(){
        return Configurator.getInstance().getLatterConfigs();
    }

    public static Context getApplicationContext(){
        return (Context) getConfigurations().get(ConfigType.APPLICATION_CONTEXT.name());
    }
}
