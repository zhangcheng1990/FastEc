package com.example.latter.app;

import java.util.WeakHashMap;

/**
 * Created by cc on 2017/9/5.
 */

public class Configurator {

    public static final WeakHashMap<String,Object> LATTER_CONFIGS= new WeakHashMap<>();

    private Configurator() {
        LATTER_CONFIGS.put(ConfigType.CONFIG_READY.name(),false);
    }

    public final WeakHashMap<String,Object> getLatterConfigs(){
        return LATTER_CONFIGS;
    }

    private static class Holder{
        private static final Configurator INSTANCE = new Configurator();
    }

    public static Configurator getInstance(){
        return Holder.INSTANCE;
    }

    public final void configure(){
        LATTER_CONFIGS.put(ConfigType.CONFIG_READY.name(),true);
    }

    public final Configurator withApi(String host){
        LATTER_CONFIGS.put(ConfigType.API_HOST.name(),host);
        return this;
    }

    private void checkConfiguration(){
        final boolean isReady = (boolean) LATTER_CONFIGS.get(ConfigType.CONFIG_READY.name());
        if (!isReady){
            throw new RuntimeException("Configuration is not ready,call configure");
        }
    }

    @SuppressWarnings("uncheck")
    final <T> T getConfiguration(Enum<ConfigType> key){
        checkConfiguration();
        return (T) LATTER_CONFIGS.get(key.name());
    }
}
