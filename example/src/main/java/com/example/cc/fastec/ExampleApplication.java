package com.example.cc.fastec;

import android.app.Application;

import com.example.latter.app.Latte;

/**
 * Created by cc on 2017/9/5.
 */

public class ExampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Latte.init(this)
                .withApi("http://127.0.0.1/")
                .configure();
    }
}
